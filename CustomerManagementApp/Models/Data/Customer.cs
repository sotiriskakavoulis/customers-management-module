using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerManagementApp.Models.Data;

[Table("customer")]
public class Customer
{
  [Column("actor_id")] [Key] public string? ActorId { get; set; }
  public bool? Deleted { get; set; }
  public string? Name { get; set; }
  public string? Surname { get; set; }
  public string? Telephone { get; set; }
  public string? Mail { get; set; }
  public DateTime? CreatedTime { get; set; }
  [Column("reservations_counter")] public int? ReservationsCounter { get; set; }
  [Column("notshown_counter")] public int? NotshownCounter { get; set; }
  [Column("restaurant_id")] public string? RestaurantId { get; set; }
  public string? Typeofcustomer { get; set; }
  [Column("cancelled_counter")] public int? CancelledCounter { get; set; }
  [Column("smokingarea")] public bool? SmokingArea { get; set; }
  public string? Note { get; set; }
  public string? Allergy { get; set; }
  public string? Dislike { get; set; }
  public string? Mylike { get; set; }
  [Column("timeperformed")] public long? TimePerformedRaw { get; set; }

  public DateTime? TimePerformed => TimePerformedRaw > 0
    ? DateTimeOffset.FromUnixTimeMilliseconds(TimePerformedRaw.Value).DateTime
    : null;

  [Column("cust_group")] public string? CustGroup { get; set; }
  [Column("cust_job")] public string? CustJob { get; set; }
  [Column("cust_company")] public string? CustCompany { get; set; }
  [Column("home_phone")] public string? HomePhone { get; set; }
  [Column("office_phone")] public string? OfficePhone { get; set; }
  [Column("other_phone")] public string? OtherPhone { get; set; }
  [Column("last_visit")] public long? LastVisitRaw { get; set; }

  public DateTime? LastVisit => LastVisitRaw > 0
    ? DateTimeOffset.FromUnixTimeMilliseconds(LastVisitRaw.Value).DateTime
    : null;

  [Column("before_last_visit")] public bool? BeforeLastVisit { get; set; }
  public string? Spender { get; set; }
  [Column("favourite_tables")] public string? FavouriteTables { get; set; }
  [Column("office_mail")] public string? OfficeMail { get; set; }
  [Column("other_mail")] public string? OtherMail { get; set; }
  [Column("company_vat")] public string? CompanyVat { get; set; }
  [Column("birth_day")] public string? BirthDay { get; set; }
  public string? Nationality { get; set; }
  public string? Country { get; set; }
  public string? City { get; set; }
  public string? Region { get; set; }
  public string? Address { get; set; }
  public string? Zipcode { get; set; }
  [Column("is_married")] public bool? IsMarried { get; set; }
  [Column("has_baby")] public bool? HasBaby { get; set; }
  [Column("Is_tourist")] public bool? IsTourist { get; set; }
  [Column("communication_language")] public string? CommunicationLanguage { get; set; }
  [Column("rejected_counter")] public int? RejectedCounter { get; set; }
  [Column("typeofmodification")] public string? TypeOfModification { get; set; }
  [Column("external_id")] public string? ExternalId { get; set; }

  public bool? ConsentStatus { get; set; }
  public string? ConsentLog { get; set; }
}

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerManagementApp.Models.Data;

[Table("saved_filters")]
public class SavedFilters
{
  [Key] public Guid Id { get; set; }

  [Required]
  [Column("restaurant_id")]
  [MaxLength(10)]
  public string RestaurantId { get; set; } = string.Empty;

  [Column("global_equality")] public GlobalEquality GlobalEquality { get; set; }
  [MaxLength(250)] public string Name { get; set; } = string.Empty;
  [Column("created_at")] public DateTime CreatedAt { get; set; }
  [Column("updated_at")] public DateTime UpdatedAt { get; set; }

  [Column(TypeName = "text")] public string Filters { get; set; } = string.Empty;
  [Column("read_only")] public bool ReadOnly { get; set; } = false;
}

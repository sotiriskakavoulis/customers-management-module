namespace CustomerManagementApp.Models;

[Serializable]
public class ResponseStatus
{
  public bool Status { get; set; }
  public string? ErrorCode { get; set; }
  public string? ErrorDescription { get; set; }

  public ResponseStatus()
  {
  }

  public ResponseStatus(ResponseStatus from)
  {
    Status = from.Status;
    ErrorCode = from.ErrorCode;
    ErrorDescription = from.ErrorDescription;
  }
}

using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CustomerManagementApp.Models;

[Serializable]
public class ColumnFilter
{
  public ColumnFilter()
  {
  }

  public ColumnFilter(string property, Equality equality, string value)
  {
    Property = property;
    Equality = equality;
    Value = value;
  }

  public string? Property { get; set; }

  [JsonConverter(typeof(JsonStringEnumConverter))]
  public Equality? Equality { get; set; }

  public string? Value { get; set; }
}

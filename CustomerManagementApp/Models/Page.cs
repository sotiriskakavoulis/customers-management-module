namespace CustomerManagementApp.Models;

[Serializable]
public class Page
{
  public int Size { get; set; }
  public int TotalElements { get; set; }
  public int TotalPages { get; set; }
  public int PageNumber { get; set; }
  public string? SortProp { get; set; }
  public string? SortDirection { get; set; }
}

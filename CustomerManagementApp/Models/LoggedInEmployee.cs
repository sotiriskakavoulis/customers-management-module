namespace CustomerManagementApp.Models;

[Serializable]
public class LoggedInEmployee
{
  public string? EmployeeId { get; set; }
  public string? Surname { get; set; }
  public string? Name { get; set; }
  public string? Type { get; set; }
  public string? Mail { get; set; }
  public string? Telephone { get; set; }
  public string[]? WritePermissions { get; set; }
  public string[]? ReadPermissions { get; set; }
  public string[]? ChainAcoun { get; set; }
}

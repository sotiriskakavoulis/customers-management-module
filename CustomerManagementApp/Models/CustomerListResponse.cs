using CustomerManagementApp.Models.Data;

namespace CustomerManagementApp.Models;

[Serializable]
public class CustomerListResponse
{
  public Customer[]? Data { get; set; }
  public Page? Page { get; set; }
}

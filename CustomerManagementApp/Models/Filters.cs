namespace CustomerManagementApp.Models;

[Serializable]
public class Filters
{
  public string? FilterText { get; set; }
  public ColumnFilter[]? ColumnFilters { get; set; }
  public GlobalEquality GlobalEquality { get; set; } = GlobalEquality.Or;
  public Dictionary<string, bool>? EnabledColumns { get; set; }
}

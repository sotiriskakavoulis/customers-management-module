namespace CustomerManagementApp.Models;

[Serializable]
public class UserMetadata
{
  public Page? Page { get; set; }
  public string? FilterText { get; set; }
  public FilterValue[]? ColumnFilters { get; set; }
  public Dictionary<string, bool>? EnabledColumns { get; set; }
  public string? GlobalEquality { get; set; }
}

namespace CustomerManagementApp.Models;

[Serializable]
public class Column
{
  public string? Name { get; set; }

  public string? Prop { get; set; }

  public string? Type { get; set; }
}

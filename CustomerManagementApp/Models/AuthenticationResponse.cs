namespace CustomerManagementApp.Models;

[Serializable]
public class AuthenticationResponse : ResponseStatus
{
  public string? Name { get; set; }
  public string? Surname { get; set; }
  public string? Email { get; set; }
  public string? RestaurantId { get; set; }
}

using CustomerManagementApp.Models;

namespace CustomerManagementApp.Models;

public class SavedFilters
{
  public Guid? Id { get; set; }
  public string Name { get; set; } = string.Empty;
  public List<FilterValue> Filters { get; set; } = new List<FilterValue>();
  public DateTime CreatedAt { get; set; }
  public DateTime UpdatedAt { get; set; }
  public bool ReadOnly { get; set; }
}

namespace CustomerManagementApp.Models;

[Serializable]
public class LoginResponse : ResponseStatus
{
  public Restaurant[]? RestaurantsList { get; set; }
  public LoggedInEmployee? LoggedInemployee { get; set; }
}

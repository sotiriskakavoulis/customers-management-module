namespace CustomerManagementApp.Models;

public enum ColumnValueType
{
  String,
  Date,
  Number,
  Boolean
}

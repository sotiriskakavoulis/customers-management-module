namespace CustomerManagementApp.Models;

[Serializable]
public class FilterValue
{
  public long Id { get; set; }
  public Column? ColumnProps { get; set; }
  public Equality? Equality { get; set; }
  public string? Value { get; set; }
}

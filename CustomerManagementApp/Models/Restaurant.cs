namespace CustomerManagementApp.Models;

[Serializable]
public class Restaurant
{
  public string? RestaurantId { get; set; }
  public string? Title { get; set; }
  public string? Info { get; set; }
  public string? Country { get; set; }
}

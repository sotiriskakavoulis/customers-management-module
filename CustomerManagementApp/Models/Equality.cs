namespace CustomerManagementApp.Models;

public enum Equality
{
  Equals,
  Contains,
  IsNot,
  GreaterThan,
  GreaterThanOrEqual,
  LessThan,
  LessThanOrEqual,
}

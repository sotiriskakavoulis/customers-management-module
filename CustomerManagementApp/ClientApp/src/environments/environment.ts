import { Environment } from "./Environment.type";

export const environment: Environment = {
  production: false,
  apiUrl: '/api',
};

import { ColumnType } from './ColumnType';


export interface Column {
  name: string;
  prop: string;
  type: ColumnType;
}

import { Column } from './Column';
import { Equality } from './Equality';


export interface FilterValue {
  id: number;
  columnProps?: Column;
  equality: Equality;
  value?: string;
}

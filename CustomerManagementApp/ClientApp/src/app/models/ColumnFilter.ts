import { EqualityEnum } from './EqualityEnum';


export interface ColumnFilter {
  property: string;
  equality: EqualityEnum;
  value: string;
}

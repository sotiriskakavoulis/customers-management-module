export enum EqualityEnum {
  equals = 0,
  contains = 1,
  isNot = 2,
  greaterThan = 3,
  greaterThanOrEqual = 4,
  lessThan = 5,
  lessThanOrEqual = 6,
}

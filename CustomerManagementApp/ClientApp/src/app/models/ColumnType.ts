
export type ColumnType = 'string' | 'date' | 'boolean' | 'number';

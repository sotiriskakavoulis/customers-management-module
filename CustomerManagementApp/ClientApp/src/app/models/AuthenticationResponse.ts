import { Authentication } from "./Authentication";
import { ResponseStatus } from "./ResponseStatus";


export interface AuthenticationResponse
  extends Authentication, ResponseStatus {
}

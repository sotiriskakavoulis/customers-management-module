export interface Authentication {
  name?: string;
  surname?: string;
  email?: string;
  restaurantId?: string;
}

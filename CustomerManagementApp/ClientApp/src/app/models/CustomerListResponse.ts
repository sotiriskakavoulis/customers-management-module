import { Page } from './page';
import { Customer } from './Customer';


export interface CustomerListResponse {
  data: Customer[];
  page: Page;
}

import { FilterValue } from './FilterValue'
import { SavedFilters } from './SavedFilters'
import { Page } from './page'

export interface Metadata {
  page: Page
  filterText?: string
  enabledColumns?: { [key: string]: boolean }
  globalEquality?: 'or' | 'and'
  activeSavedFiltersIndex: number
}

export interface OptionalMetadata
  extends Omit<Metadata, 'page' | 'activeSavedFiltersIndex'> {
  page?: Page
  activeSavedFiltersIndex?: number
}

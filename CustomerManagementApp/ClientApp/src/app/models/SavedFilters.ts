import { FilterValue } from 'src/app/models/FilterValue'

export interface SavedFilters {
  id: string
  name: string
  filters: FilterValue[]
  createdAt: Date
  updatedAt: Date
  readOnly?: boolean
}

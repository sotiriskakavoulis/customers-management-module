
export interface ResponseStatus {
  status: boolean;
  errorCode: string;
  errorDescription: string;
}



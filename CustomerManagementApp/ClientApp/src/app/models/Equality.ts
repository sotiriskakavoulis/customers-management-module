export type Equality =
  | 'equals'
  | 'contains'
  | 'isNot'
  | 'greaterThan'
  | 'greaterThanOrEqual'
  | 'lessThan'
  | 'lessThanOrEqual'



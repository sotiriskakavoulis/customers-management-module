import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { BehaviorSubject, lastValueFrom } from 'rxjs'
import { SavedFilters } from '../models/SavedFilters'
import { Page } from '../models/page'
import { Metadata, OptionalMetadata } from '../models/Metadata'
import { EqualityEnum } from '../models/EqualityEnum'
import { FilterValue } from '../models/FilterValue'
import * as moment from 'moment'
import { generateUUID } from '../utils'

@Injectable({
  providedIn: 'root',
})
export class MetadataService {
  defaultSavedFiltersName = 'Default'

  hasUnsavedChanges$ = new BehaviorSubject<boolean>(false)
  loader$ = new BehaviorSubject<'visible' | 'hidden'>('hidden')
  metadata$ = new BehaviorSubject<Metadata>({
    page: new Page(),
    activeSavedFiltersIndex: 0,
  })
  savedFilters$ = new BehaviorSubject<SavedFilters[]>([
    {
      id: generateUUID(),
      name: this.defaultSavedFiltersName,
      filters: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      readOnly: true,
    },
  ])

  get activeSavedFiltersIndex() {
    return this.metadata$.getValue().activeSavedFiltersIndex
  }

  constructor(private http: HttpClient) {
    const metadata = this.fetch()
    this.metadata$.next(metadata)
  }

  updateMetadata(
    metadata: OptionalMetadata,
    hasUnsavedChanges: boolean = true,
  ): Metadata {
    const combinedMetadata = this.combineWithCurrentMetadata(metadata)
    this.hasUnsavedChanges$.next(hasUnsavedChanges)

    this.metadata$.next(combinedMetadata)
    localStorage.setItem('metadata', JSON.stringify(combinedMetadata))

    return combinedMetadata
  }

  updateColumnFilters(
    savedFilters: FilterValue[],
    hasUnsavedChanges: boolean = true,
  ): SavedFilters[] {
    const currentSavedFiltersList = this.savedFilters$.getValue()
    const currentSavedFilters =
      currentSavedFiltersList[this.activeSavedFiltersIndex]
    currentSavedFilters.filters = savedFilters
    if (hasUnsavedChanges) {
      currentSavedFilters.updatedAt = moment().local().toDate()
    }

    this.savedFilters$.next(currentSavedFiltersList)
    this.updateMetadata({}, hasUnsavedChanges)

    return currentSavedFiltersList
  }

  duplicateFilters(restaurantId: string, savedFilters: SavedFilters) {
    const currentSavedFiltersList = this.savedFilters$.getValue()
    currentSavedFiltersList.push(savedFilters)

    this.loader$.next('visible')
    return this.saveFilters(restaurantId)
      .then((filters) => {
        this.savedFilters$.next(filters)
        this.updateMetadata(
          {
            activeSavedFiltersIndex: filters.length - 1,
          },
          false,
        )
      })
      .finally(() => {
        this.loader$.next('hidden')
      })
  }

  removeSavedFilters(restaurantId: string, filtersId: string) {
    const currentSavedFilters = this.savedFilters$.getValue()
    const updatedSavedFilters = currentSavedFilters.filter(
      (filter) => filter.id !== filtersId,
    )
    this.savedFilters$.next(updatedSavedFilters)

    this.loader$.next('visible')
    return this.saveFilters(restaurantId)
      .then((filters) => {
        this.savedFilters$.next(filters)
        this.updateMetadata(
          {
            activeSavedFiltersIndex: 0,
          },
          false,
        )
      })
      .finally(() => {
        this.loader$.next('hidden')
      })
  }

  saveFilters(restaurantId: string) {
    const url = `api/metadata/${restaurantId}/saved_filters`
    const filters = mapToCurrentSavedFiltersRequest(
      this.savedFilters$.getValue(),
    )

    this.loader$.next('visible')
    const res = lastValueFrom(this.http.post<SavedFilters[]>(url, filters))
    return res
      .then((filters) => {
        const mappedFilters = mapToCurrentSavedFiltersRequest(filters)
        this.savedFilters$.next(mappedFilters)
        this.updateMetadata({}, false)
        return filters
      })
      .catch((err) => filters)
      .finally(() => {
        this.loader$.next('hidden')
      })
  }

  fetchFilters(restaurantId: string) {
    const url = `api/metadata/${restaurantId}/saved_filters`

    this.loader$.next('visible')
    const res = lastValueFrom(this.http.get<SavedFilters[]>(url))
    return res
      .then((filters) => {
        if (!filters.length) {
          return filters
        }

        const mappedFilters = mapToCurrentSavedFiltersRequest(filters)
        this.savedFilters$.next(mappedFilters)
        this.updateMetadata({}, false)

        return filters
      })
      .catch((err) => {})
      .finally(() => {
        this.loader$.next('hidden')
      })
  }

  renameSavedFilters(restaurantId: string, newName: string) {
    const currentSavedFiltersList = this.savedFilters$.getValue()
    const currentSavedFilters =
      currentSavedFiltersList[this.activeSavedFiltersIndex]
    currentSavedFilters.name = newName
    // this.savedFilters$.next(currentSavedFilters)

    return this.saveFilters(restaurantId)
  }

  private fetch(): Metadata {
    const metadataText = localStorage.getItem('metadata')
    if (!metadataText) {
      return this.metadata$.getValue()
    }

    const metadata = JSON.parse(metadataText) as Metadata
    this.metadata$.next(metadata)

    return metadata
  }

  private combineWithCurrentMetadata(metadata: OptionalMetadata) {
    const currentMetadata = this.metadata$.getValue()
    const combinedMetadata = {
      page: metadata.page ?? currentMetadata.page,
      filterText: metadata.filterText ?? currentMetadata.filterText,
      enabledColumns: metadata.enabledColumns ?? currentMetadata.enabledColumns,
      globalEquality: metadata.globalEquality ?? currentMetadata.globalEquality,
      activeSavedFiltersIndex:
        metadata.activeSavedFiltersIndex ?? this.activeSavedFiltersIndex,
    }
    return combinedMetadata
  }
}
function mapToCurrentSavedFiltersRequest(filters: SavedFilters[]): any[] {
  return filters.map((savedFilter) => ({
    id: savedFilter.id,
    name: savedFilter.name,
    filters: savedFilter.filters.map((filter) => ({
      id: filter.id,
      columnProps: filter.columnProps,
      equality: EqualityEnum[filter.equality],
      value: filter.value,
    })),
    createdAt: savedFilter.createdAt,
    updatedAt: moment(savedFilter.updatedAt).local(),
    readOnly: savedFilter.readOnly,
  }))
}

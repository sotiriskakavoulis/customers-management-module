import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, lastValueFrom } from 'rxjs';
import { environment } from '../../environments/environment';
import { AuthenticationResponse } from '../models/AuthenticationResponse';
import { Authentication } from '../models/Authentication';
import { ResponseStatus } from '../models/ResponseStatus';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public userInfo = new BehaviorSubject<Authentication | undefined>(undefined);

  constructor(private http: HttpClient) {
    this.isAuthenticated();
  }

  async login(
    username: string,
    password: string
  ): Promise<{ success: boolean; errorCode?: string; errorMessage?: string }> {
    const url = `${environment.apiUrl}/auth/login`;

    const body = {
      username,
      password,
    };

    const resp = this.http.post<AuthenticationResponse>(url, body);
    const result = await lastValueFrom<AuthenticationResponse>(resp).catch(
      (err) => {
        return {
          status: false,
          errorCode: err.statusText,
          errorDescription: err.message,
        } as AuthenticationResponse;
      }
    );

    this.userInfo.next({
      name: result?.name,
      surname: result?.surname,
      email: result?.email,
      restaurantId: result?.restaurantId,
    });
    if (result.status) {
      return { success: true };
    }
    return {
      success: false,
      errorCode: result.errorCode,
      errorMessage: result.errorDescription,
    };
  }

  async isAuthenticated(): Promise<boolean> {
    const url = `${environment.apiUrl}/auth/is_authenticated`;
    const resp = this.http.get<AuthenticationResponse>(url);
    const result = await lastValueFrom<AuthenticationResponse>(resp).catch(
      (err) => {
        return {
          status: false,
          errorCode: err.statusText,
          errorDescription: err.message,
        } as AuthenticationResponse;
      }
    );

    this.userInfo.next({
      name: result.name,
      surname: result.surname,
      email: result.email,
      restaurantId: result.restaurantId,
    });

    return result.status;
  }

  async logout(): Promise<void> {
    const url = `${environment.apiUrl}/auth/logout`;
    const resp = this.http.post<ResponseStatus>(url, {});
    const result = await lastValueFrom<ResponseStatus>(resp).catch((err) => {
      return {
        status: false,
        errorCode: err.statusText,
        errorDescription: err.message,
      } as AuthenticationResponse;
    });
    if (result.status) {
      this.userInfo.next({});
    }
  }
}

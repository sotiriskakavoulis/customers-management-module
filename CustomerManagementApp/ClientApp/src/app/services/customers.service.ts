import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { lastValueFrom } from 'rxjs'
import { ColumnFilter } from '../models/ColumnFilter'
import { CustomerListResponse } from '../models/CustomerListResponse'
import { Page } from '../models/page'
import { GlobalEqualityEnum } from '../models/GlobalEqualityEnum'

@Injectable({
  providedIn: 'root',
})
export class CustomersService {
  constructor(private http: HttpClient) {}

  async getCustomers(
    restaurantId: string,
    filterText: string,
    page: Page,
    columnFilters: ColumnFilter[],
    globalEquality?: 'or' | 'and',
  ): Promise<CustomerListResponse> {
    const url = this.generateUrl(
      `api/customer/for_restaurant/${restaurantId}?`,
      page,
    )
    const body = {
      filterText,
      columnFilters,
      globalEquality: GlobalEqualityEnum[globalEquality ?? 'or'],
    }

    const res = lastValueFrom(this.http.post<CustomerListResponse>(url, body))
    return res.catch((err) => ({
      data: [],
      page: {
        size: 0,
        totalElements: 0,
        totalPages: 0,
        pageNumber: 0,
      },
    }))
  }

  private generateUrl(baseUrl: string, page: Page) {
    const urlParts = [
      baseUrl,
      `pageSize=${page.size}`,
      `&pageNumber=${page.pageNumber}`,
      page.sortProp ? `&sortProp=${page.sortProp}` : undefined,
      page.sortDirection ? `&sortDirection=${page.sortDirection}` : undefined,
    ].filter((x) => x)
    const url = urlParts.join('')
    return url
  }

  async exportCustomers(
    restaurantId: string,
    filterText: string,
    page: Page,
    columnFilters: ColumnFilter[],
    globalEquality: 'or' | 'and' | undefined,
    enabledColumns: { [key: string]: boolean },
  ): Promise<void> {
    const url = this.generateUrl(`api/file/for_restaurant/${restaurantId}?`, {
      sortProp: page.sortProp,
      sortDirection: page.sortDirection,
    })
    const body = JSON.stringify({
      filterText,
      columnFilters,
      globalEquality: GlobalEqualityEnum[globalEquality ?? 'or'],
      enabledColumns,
    })
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
    }

    return fetch(url, {
      method: 'POST',
      body,
      headers,
    })
      .then((response) => response.blob())
      .then((blob) => {
        // Create a link element, use the blob as the href
        const url = window.URL.createObjectURL(blob)
        const a = document.createElement('a')
        a.href = url
        a.download = 'MyWorkbook.xlsx' // the filename you want
        document.body.appendChild(a)
        a.click()

        // Clean up and remove the link
        a.remove()
        window.URL.revokeObjectURL(url)
      })
      .catch((e) => console.error(e))
  }
}

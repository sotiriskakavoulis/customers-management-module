import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  createUrlTreeFromSnapshot,
} from '@angular/router';
import { AuthenticationService } from './authentication.service';

export const authGuard = async (next: ActivatedRouteSnapshot) => {
  return (await inject(AuthenticationService).isAuthenticated())
    ? true
    : createUrlTreeFromSnapshot(next, ['/', 'login']);
};

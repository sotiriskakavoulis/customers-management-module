import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core'
import { FormsModule } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon'
import { MatInputModule } from '@angular/material/input'

@Component({
  selector: 'app-editable-label',
  standalone: true,
  imports: [
    FormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
  ],
  templateUrl: './editable-label.component.html',
  styleUrl: './editable-label.component.css',
})
export class EditableLabelComponent {
  @Input() value: string | undefined = ''
  @Output() valueChange = new EventEmitter<string>()

  private initialValue = ''

  mode: 'view' | 'edit' = 'view'

  ngOnChanges() {
    this.initialValue = this.value ?? ''
  }

  edit() {
    this.initialValue = this.value ?? ''
    this.mode = 'edit'
  }

  accept() {
    this.mode = 'view'
    this.initialValue = this.value ?? ''
    this.valueChange.emit(this.value ?? '')
  }

  cancel() {
    this.mode = 'view'
    this.value = this.initialValue
  }
}

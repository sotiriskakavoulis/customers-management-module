import { Component, EventEmitter, Input, Output } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { MatDialog, MatDialogModule } from '@angular/material/dialog'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { InputPromptDialogComponent } from './input-prompt-dialog/input-prompt-dialog.component'

@Component({
  selector: 'app-input-prompt',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
  ],
  templateUrl: './input-prompt.component.html',
  styleUrl: './input-prompt.component.css',
})
export class InputPromptComponent {
  @Input() label: string = ''
  @Input() modalTitle: string = ''
  @Input() modalPrompt: string = ''
  @Input() inputLabel: string = ''
  @Input() inputDefaultValue: string = ''
  @Input() disabled: boolean = false

  @Output() submit = new EventEmitter<string>()
  @Output() abort = new EventEmitter<void>()

  value: string = ''

  constructor(public dialog: MatDialog) {}

  openDialog() {
    const dialogRef = this.dialog.open(InputPromptDialogComponent, {
      data: {
        title: this.modalTitle,
        prompt: this.modalPrompt,
        label: this.inputLabel,
        inputDefaultValue: this.inputDefaultValue,
      },
    })

    dialogRef.afterClosed().subscribe((res) => {
      if (res === false) {
        this.abort.next()
        return
      }
      this.submit.next(res)
    })
  }
}

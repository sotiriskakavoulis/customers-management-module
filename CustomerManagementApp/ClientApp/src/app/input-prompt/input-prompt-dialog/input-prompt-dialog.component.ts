import { Component, EventEmitter, Inject, Input, Output } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'

@Component({
  selector: 'app-input-prompt-dialog',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
  ],
  templateUrl: './input-prompt-dialog.component.html',
  styleUrl: './input-prompt-dialog.component.css',
})
export class InputPromptDialogComponent {
  @Output() submit = new EventEmitter<string>()

  title: string = ''
  prompt: string = ''
  label: string = ''
  value: string = ''

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    if (data) {
      this.title = data.title
      this.prompt = data.prompt
      this.label = data.label
      this.value = data.inputDefaultValue
    }
  }
}

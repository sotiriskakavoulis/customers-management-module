import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideHttpClient } from '@angular/common/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(),
    { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] },
    importProvidersFrom(
      NgxDatatableModule.forRoot({
        messages: {
          emptyMessage: 'No data to display', // Message to show when array is presented, but contains no values
          totalMessage: 'total', // Footer total message
          selectedMessage: 'selected', // Footer selected message
        },
      })
    ),
    provideAnimationsAsync(),
  ],
};

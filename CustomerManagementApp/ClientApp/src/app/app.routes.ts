import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

import { CustomersComponent } from './customers/customers.component';
import { authGuard } from './services/auth-guard';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'customers',
    component: CustomersComponent,
    canActivate: [authGuard],
  },
  {
    path: '*',
    redirectTo: '/',
    pathMatch: 'full',
  },
];

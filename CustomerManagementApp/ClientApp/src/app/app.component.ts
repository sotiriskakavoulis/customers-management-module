import { CommonModule } from '@angular/common'
import { Component } from '@angular/core'
import { MatButtonModule } from '@angular/material/button'
import { MatToolbarModule } from '@angular/material/toolbar'
import { Router, RouterOutlet } from '@angular/router'
import { AuthenticationService } from './services/authentication.service'
import { MatIconModule } from '@angular/material/icon'

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  employeeName: string | undefined
  title = 'customer-marketing-tool'

  constructor(
    private auth: AuthenticationService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.auth.userInfo.subscribe((userInfo) => {
      if (userInfo?.name) {
        this.employeeName = `${userInfo.name} ${userInfo.surname}`
      } else {
        this.employeeName = undefined
      }
    })
  }

  ngOnDestroy(): void {
    this.auth.userInfo.unsubscribe()
  }

  async logout(): Promise<void> {
    await this.auth.logout()
    this.router.navigate(['/login'])
  }
}

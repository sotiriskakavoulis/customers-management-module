import { AsyncPipe, CommonModule } from '@angular/common'
import { Component, EventEmitter, Input, Output } from '@angular/core'
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms'
import {
  MatAutocompleteModule,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatSelectModule } from '@angular/material/select'
import { Observable, map, startWith } from 'rxjs'
import { Column } from '../../models/Column'
import { FilterValue } from '../../models/FilterValue'

@Component({
  selector: 'app-columns-select',
  standalone: true,
  imports: [
    FormsModule,
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    ReactiveFormsModule,
    AsyncPipe,
  ],
  templateUrl: './columns-select.component.html',
  styleUrl: './columns-select.component.css',
})
export class ColumnsSelectComponent {
  @Input() columns?: Column[]
  @Input() filterValue?: FilterValue
  @Output() valueChange = new EventEmitter<FilterValue>()

  searchInputControl = new FormControl('')
  filteredColumns: Observable<Column[]> = new Observable<Column[]>()

  ngOnInit() {
    if (this.filterValue?.columnProps) {
      this.searchInputControl.setValue(this.filterValue?.columnProps?.name)
    }
    this.filteredColumns = this.searchInputControl.valueChanges.pipe(
      startWith(''),
      map((value) => this.filter(value || '')),
    )
  }

  emitValue(event: MatAutocompleteSelectedEvent) {
    const column = this.columns?.find((c) => c.name === event.option.value)
    if (!this.filterValue || !column) {
      return
    }

    this.valueChange.emit({
      id: this.filterValue?.id,
      equality: 'equals',
      columnProps: column,
    })
  }

  private filter(value: string): Column[] {
    const filterValue = value.toLowerCase()

    return (
      this.columns?.filter((column) =>
        column.name.toLowerCase().includes(filterValue),
      ) ?? []
    )
  }
}

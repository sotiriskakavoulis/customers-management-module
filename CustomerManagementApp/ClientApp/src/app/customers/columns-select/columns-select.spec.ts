import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnsSelectComponent } from './columns-select';

describe('NoTypeFilterComponent', () => {
  let component: ColumnsSelectComponent;
  let fixture: ComponentFixture<ColumnsSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ColumnsSelectComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColumnsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, Output } from '@angular/core'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatSelectModule } from '@angular/material/select'
import { FilterValue } from '../../models/FilterValue'
import { Equality } from '../../models/Equality'
import { Column } from '../../models/Column'
import { CommonModule } from '@angular/common'
import { ColumnsSelectComponent } from '../columns-select/columns-select'
import { FormsModule } from '@angular/forms'
import { EqualitySelectComponent } from '../equality-select/equality-select.component'
import { MatInputModule } from '@angular/material/input'

@Component({
  selector: 'app-input-filter',
  standalone: true,
  imports: [
    FormsModule,
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    ColumnsSelectComponent,
    EqualitySelectComponent,
  ],
  templateUrl: './input-filter.component.html',
  styleUrl: './input-filter.component.css',
})
export class InputFilterComponent {
  @Input() columns?: Column[]
  @Input() filterValue?: FilterValue
  @Input() type: 'text' | 'number' = 'text'
  @Output() valueChange = new EventEmitter<FilterValue>()

  inputValue: string = ''

  ngOnInit() {
    if (typeof this.filterValue?.value === 'string') {
      this.inputValue = this.filterValue?.value ?? ''
    }
  }

  emitValue(value: FilterValue) {
    this.valueChange.emit({
      ...value,
    })
  }

  changeEquality(value: Equality) {
    if (!this.filterValue) {
      return
    }

    this.valueChange.emit({
      ...this.filterValue,
      equality: value,
    })
  }

  updateValue() {
    if (!this.filterValue) {
      return
    }

    this.valueChange.emit({
      ...this.filterValue,
      value: this.inputValue,
    })
  }
}

export interface SortConfig {
  dir?: 'asc' | 'desc';
  prop?: string;
}

export interface ColumnConfig {
  prop: string;
  $$id: string;
  name: string;
  resizable: boolean;
  sortable: boolean;
  draggable: boolean;
  canAutoResize: boolean;
  width: number;
  isTreeColumn: boolean;
  dragging: boolean;
}

export interface TableConfig {
  sorts: SortConfig[];
  column: ColumnConfig;
  newValue: string;
}

import { CommonModule } from '@angular/common'
import { Component, ViewChild } from '@angular/core'
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import {
  MatCheckboxChange,
  MatCheckboxModule,
} from '@angular/material/checkbox'
import { MatDialog, MatDialogModule } from '@angular/material/dialog'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatGridListModule } from '@angular/material/grid-list'
import { MatInputModule } from '@angular/material/input'
import { MatMenuModule } from '@angular/material/menu'
import { MatOption, MatSelect, MatSelectChange } from '@angular/material/select'
import { MatTooltipModule } from '@angular/material/tooltip'
import { NgxDatatableModule, TableColumnProp } from '@swimlane/ngx-datatable'
import { ColumnFilter } from '../models/ColumnFilter'
import { Customer } from '../models/Customer'
import { EqualityEnum } from '../models/EqualityEnum'
import { FilterValue } from '../models/FilterValue'
import { Page } from '../models/page'
import { AuthenticationService } from '../services/authentication.service'
import { CustomersService } from '../services/customers.service'
import { MetadataService } from '../services/metadata.service'
import { Metadata } from '../models/Metadata'
import { FiltersComponent } from './filters/filters.component'
import { ColumnDefinition, GridComponent } from './grid/grid.component'
import { MatSidenavModule } from '@angular/material/sidenav'
import { combineLatest } from 'rxjs'

@Component({
  selector: 'app-customers',
  standalone: true,
  imports: [
    NgxDatatableModule,
    FormsModule,
    ReactiveFormsModule,
    GridComponent,
    MatButtonModule,
    CommonModule,
    MatTooltipModule,
    MatInputModule,
    MatFormFieldModule,
    MatMenuModule,
    MatCardModule,
    MatCheckboxModule,
    MatGridListModule,
    MatDialogModule,
    MatSelect,
    MatOption,
    MatSidenavModule,
  ],
  templateUrl: './customers.component.html',
  styleUrl: './customers.component.scss',
})
export class CustomersComponent {
  overrideRestaurantId: string = ''
  restaurantId: string = ''
  filtersPreview: string = ''
  showFiller = false
  metadata: Metadata = { page: new Page(), activeSavedFiltersIndex: 0 }
  columnFilters: FilterValue[] = []
  loading: boolean = false
  rows: Customer[] = []
  columnDefs: ColumnDefinition[] = [
    {
      prop: 'actorId',
      width: 220,
      type: 'string',
    },
    { prop: 'deleted', width: 80, type: 'boolean' },
    { prop: 'name', width: 120, type: 'string' },
    { prop: 'surname', width: 170, type: 'string' },
    { prop: 'telephone', width: 150, type: 'string' },
    { prop: 'mail', width: 250, type: 'string' },
    {
      prop: 'createdTime',
      width: 180,
      type: 'date',
    },
    {
      prop: 'reservationsCounter',
      name: 'Reservations Counter',
      width: 100,
      type: 'number',
    },
    {
      prop: 'notshownCounter',
      name: 'Not Shown Counter',
      width: 100,
      type: 'number',
    },
    {
      prop: 'typeofcustomer',
      name: 'Type Of Customer',
      width: 100,
      type: 'string',
    },
    {
      prop: 'cancelledCounter',
      name: 'Cancelled Counter',
      width: 100,
      type: 'number',
    },
    { prop: 'smokingArea', name: 'Smoking Area', width: 100, type: 'boolean' },
    { prop: 'note', width: 100, type: 'string' },
    { prop: 'allergy', width: 100, type: 'string' },
    { prop: 'dislike', width: 100, type: 'string' },
    { prop: 'mylike', width: 100, type: 'string' },
    { prop: 'lastVisit', name: 'Last Visit', width: 180, type: 'date' },
    {
      prop: 'beforeLastVisit',
      name: 'Before last Visit',
      width: 100,
      type: 'boolean',
    },
    { prop: 'spender', width: 100, type: 'string' },
    {
      prop: 'favouriteTables',
      name: 'Favourite Tables',
      width: 100,
      type: 'string',
    },
    { prop: 'companyVat', name: 'Company Vat', width: 100, type: 'string' },
    { prop: 'birthDay', name: 'Birthday', width: 100, type: 'date' },
    { prop: 'nationality', width: 100, type: 'string' },
    { prop: 'country', width: 100, type: 'string' },
    { prop: 'city', width: 100, type: 'string' },
    { prop: 'region', width: 100, type: 'string' },
    { prop: 'address', width: 100, type: 'string' },
    { prop: 'zipcode', width: 100, type: 'string' },
    { prop: 'isTourist', name: 'Is Tourist', width: 100, type: 'boolean' },
    {
      prop: 'communicationLanguage',
      name: 'Communication Language',
      width: 100,
      type: 'string',
    },
    {
      prop: 'rejectedCounter',
      name: 'Rejected Counter',
      width: 100,
      type: 'number',
    },
    { prop: 'consentStatus', width: 80, type: 'boolean' },
  ]
  columnsFormGroup?: FormGroup = undefined

  get enabledColumns(): { [key: string]: boolean } {
    return this.columnDefs.reduce<{ [key: string]: boolean }>(
      (total, current) => {
        const prop = `${current.prop}`.toLocaleLowerCase()
        total[prop] = !current.hidden
        return total
      },
      {},
    )
  }

  constructor(
    private customers: CustomersService,
    private auth: AuthenticationService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private metadataService: MetadataService,
  ) {
    this.metadataService.savedFilters$.subscribe((savedFilters) => {
      this.columnFilters =
        savedFilters[this.metadata.activeSavedFiltersIndex].filters
    })
    this.metadataService.metadata$.subscribe((metadata) => {
      this.init(metadata)
    })
  }

  ngOnInit() {
    this.auth.userInfo.subscribe((userinfo) => {
      this.restaurantId = userinfo?.restaurantId ?? ''
      this.loading = true
      this.loadCustomers(this.metadata.page)
    })
  }

  ngOnDestory() {
    this.auth.userInfo.unsubscribe()
    this.metadataService.metadata$.unsubscribe()
  }

  init(metadata: Metadata) {
    this.metadata = {
      ...metadata,
      page: metadata.page ?? { size: 100, pageNumber: 0 },
    }

    const columnsMap = this.columnDefs.reduce<{ [key: string]: boolean }>(
      (result, current) => {
        const { prop } = current
        if (!prop) {
          return result
        }
        const propText = prop as string
        result[propText] =
          metadata.enabledColumns?.[propText.toLocaleLowerCase()] ?? true
        current.hidden = !result[propText]
        return result
      },
      {},
    )
    this.columnsFormGroup = this.formBuilder.group(columnsMap)
  }

  ngOnDestroy() {
    this.auth.userInfo.unsubscribe()
  }

  loadCustomers = (page: Page): Promise<void> => {
    this.loading = true

    const restaurantId = this.overrideRestaurantId || this.restaurantId
    const columnFilters: ColumnFilter[] =
      this.columnFilters.map((col) => ({
        property: col.columnProps?.prop ?? '',
        equality: EqualityEnum[col.equality],
        value: col.value ?? '',
      })) ?? []

    return this.customers
      .getCustomers(
        restaurantId,
        this.metadata.filterText ?? '',
        page,
        columnFilters,
        this.metadata.globalEquality,
      )
      .then((resp) => {
        const { data, page } = resp
        this.rows = data
        this.metadata = { ...this.metadata, page }
      })
      .catch((error) => {
        this.rows = []
      })
      .finally(() => {
        this.loading = false
      })
  }

  async export() {
    const restaurantId = this.overrideRestaurantId || this.restaurantId
    const columnFilters: ColumnFilter[] =
      this.columnFilters?.map((col) => ({
        property: col.columnProps?.prop ?? '',
        equality: EqualityEnum[col.equality],
        value: `${col.value}`,
      })) ?? []

    this.loading = true
    await this.customers.exportCustomers(
      restaurantId,
      this.metadata.filterText ?? '',
      this.metadata.page,
      columnFilters,
      this.metadata.globalEquality,
      this.enabledColumns,
    )
    this.loading = false
  }

  async updateFilter() {
    this.metadataService.updateMetadata(this.metadata)

    await this.loadCustomers(this.metadata.page)
  }

  updateColumns(evt: MatCheckboxChange, prop?: TableColumnProp) {
    const { checked } = evt
    const column = this.columnDefs.find((col) => col.prop === prop)
    if (column) {
      column.hidden = !checked
    } else {
      return
    }

    this.metadataService.updateMetadata({
      ...this.metadata,
      enabledColumns: this.enabledColumns,
    })

    this.columnDefs = [...this.columnDefs]
  }

  openFiltersDialog() {
    const dialogRef = this.dialog.open(FiltersComponent, {
      width: '1080px',
      minHeight: '600px',
      data: {
        columns: this.columnDefs.map((c) => ({
          prop: c.prop,
          name: c.name || c.prop,
          type: c.type,
        })),
      },
    })

    dialogRef.afterClosed().subscribe((res) => this.handleDialogResult(res))
  }

  handleDialogResult(
    result:
      | {
          filters: FilterValue[]
          globalEquality: 'or' | 'and'
        }
      | false,
  ) {
    if (!result) {
      return
    }

    this.metadata = {
      ...this.metadata,
      globalEquality: result.globalEquality,
    }
    // this.metadataService.updateMetadata(this.metadata)

    this.filtersPreview = result.filters.reduce((total, curr) => {
      if (!curr?.columnProps) {
        return total
      }

      const currentPreview = `${curr.columnProps?.name}:${curr.value}`
      return total
        ? `${total} ${this.metadata.globalEquality} ${currentPreview}`
        : `${currentPreview}`
    }, '')
    this.loadCustomers(this.metadata.page)
  }

  changeSize(event: MatSelectChange) {
    this.metadata.page.size = event.value
    this.metadataService.updateMetadata(this.metadata)
    this.loadCustomers(this.metadata.page)
  }

  setMetadata(metadata: Metadata) {
    this.metadata = { ...metadata }
    this.metadataService.updateMetadata(metadata)

    this.loadCustomers(this.metadata.page)
  }
}

import { Component, EventEmitter, Input, Output } from '@angular/core'
import { FilterValue } from '../../models/FilterValue'
import { Equality } from '../../models/Equality'
import { Column } from '../../models/Column'
import { ColumnsSelectComponent } from '../columns-select/columns-select'
import { EqualitySelectComponent } from '../equality-select/equality-select.component'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { CommonModule } from '@angular/common'
import { FormControl, ReactiveFormsModule } from '@angular/forms'
import { provideMomentDateAdapter } from '@angular/material-moment-adapter'
import * as moment from 'moment'
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker'

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
}

@Component({
  selector: 'app-date-filter',
  standalone: true,
  imports: [
    CommonModule,
    ColumnsSelectComponent,
    EqualitySelectComponent,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
  ],
  providers: [provideMomentDateAdapter(MY_FORMATS)],
  templateUrl: './date-filter.component.html',
  styleUrl: './date-filter.component.scss',
})
export class DateFilterComponent {
  @Input() columns?: Column[]
  @Input() filterValue?: FilterValue
  @Output() valueChange = new EventEmitter<FilterValue>()

  date: FormControl
  timeInText: string = ''

  constructor() {
    const date = new Date()
    this.date = new FormControl(moment(date))
    this.timeInText = moment(date).format('HH:mm')
  }

  ngOnChanges() {
    if (!this.filterValue) {
      return
    }

    if (typeof this.filterValue?.value === 'string') {
      const date = moment(new Date(this.filterValue.value))
      this.date = new FormControl(moment(date))
      this.timeInText = date.format('HH:mm')
    } else {
      this.valueChange.emit({
        ...this.filterValue,
        value: moment().toISOString(),
      })
    }
  }

  emitValue(value: FilterValue) {
    this.valueChange.emit({
      ...value,
    })
  }

  changeEquality(value: Equality) {
    if (!this.filterValue) {
      return
    }

    this.valueChange.emit({
      ...this.filterValue,
      equality: value,
    })
  }

  changeTime(time: string) {
    if (!this.date) {
      return
    }

    this.timeInText = time
    var [hours, minutes] = time.split(':').map(Number)
    this.date.value.set({
      hour: hours,
      minute: minutes,
    })
    this.updateValue()
  }

  updateValue() {
    if (!this.filterValue) {
      return
    }

    this.valueChange.emit({
      ...this.filterValue,
      value: this.date.value?.toISOString(),
    })
  }
}

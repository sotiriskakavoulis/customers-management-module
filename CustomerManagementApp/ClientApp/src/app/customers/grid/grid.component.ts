import { CommonModule } from '@angular/common'
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChange,
  ViewChild,
} from '@angular/core'
import { NgxDatatableModule, TableColumn } from '@swimlane/ngx-datatable'
import { Customer } from 'src/app/models/Customer'
import { Page } from 'src/app/models/page'
import { Metadata, OptionalMetadata } from 'src/app/models/Metadata'
import { SortConfig, TableConfig } from '../table-config'

export type ColumnDefinition = TableColumn & {
  hidden?: boolean
  type: 'string' | 'number' | 'boolean' | 'date'
}

@Component({
  selector: 'app-grid',
  standalone: true,
  imports: [CommonModule, NgxDatatableModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './grid.component.html',
  styleUrl: './grid.component.css',
})
export class GridComponent {
  @Input() rows: Customer[] = []
  @Input() columnDefs: ColumnDefinition[] = []
  @Input() metadata: Metadata = { page: new Page(), activeSavedFiltersIndex: 0 }
  @Input() loading: boolean = false
  @Output() updateMetadata = new EventEmitter<Metadata>()

  @ViewChild('datatable', { static: false }) myTable: any

  page: Page = { size: 100 }

  sorts: SortConfig[] = [
    {
      dir: undefined,
      prop: undefined,
    },
  ]

  constructor() {}

  ngOnChanges(changes: SimpleChange) {
    this.page = this.metadata.page
  }

  setPage(pageInfo: { offset: number }) {
    if (this.loading) {
      return
    }

    this.page.pageNumber = pageInfo.offset || 0
    this.rows = []

    this.updateAndEmitMetadata({ page: this.page })
  }

  sort(event: TableConfig) {
    const [sort] = event.sorts

    this.page.sortDirection = sort.dir
    this.page.sortProp = sort.prop
    this.myTable.offset = this.page.pageNumber

    this.updateAndEmitMetadata({
      page: { ...this.page, sortProp: sort.prop, sortDirection: sort.dir },
    })
  }

  private updateAndEmitMetadata(metadata: OptionalMetadata) {
    const combinedMetadata = { ...this.metadata, ...metadata }
    this.metadata = combinedMetadata
    this.updateMetadata.emit(combinedMetadata)
  }
}

import { Component } from '@angular/core'
import { MatIconModule } from '@angular/material/icon'
import { MatListModule } from '@angular/material/list'
import { AsyncPipe, DatePipe, NgIf } from '@angular/common'
import { MetadataService } from 'src/app/services/metadata.service'
import { MatButtonModule } from '@angular/material/button'
import { SavedFilters } from '../../models/SavedFilters'
import { AuthenticationService } from 'src/app/services/authentication.service'

@Component({
  selector: 'app-saved-filters',
  standalone: true,
  imports: [
    NgIf,
    AsyncPipe,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    DatePipe,
  ],
  templateUrl: './saved-filters.component.html',
  styleUrl: './saved-filters.component.css',
})
export class SavedFiltersComponent {
  restaurantId: string = ''

  constructor(
    private authService: AuthenticationService,
    public metadataService: MetadataService,
  ) {}

  ngOnInit() {
    const userInfo = this.authService.userInfo.getValue()
    this.restaurantId = userInfo?.restaurantId ?? ''
    if (this.restaurantId) {
      this.metadataService.fetchFilters(this.restaurantId)
    }
  }

  applyFilters(index: number, savedFilters: SavedFilters) {
    this.metadataService.updateMetadata(
      { activeSavedFiltersIndex: index },
      false,
    )
    this.metadataService.updateColumnFilters(savedFilters.filters, false)
  }

  deleteFilter(filterId: string) {
    if (this.restaurantId) {
      this.metadataService.removeSavedFilters(this.restaurantId, filterId)
    }
  }
}

import { CommonModule } from '@angular/common'
import { Component, EventEmitter, Input, Output } from '@angular/core'
import {
  MatCheckboxChange,
  MatCheckboxModule,
} from '@angular/material/checkbox'
import { MatFormFieldModule } from '@angular/material/form-field'
import { ColumnsSelectComponent } from '../columns-select/columns-select'
import { EqualitySelectComponent } from '../equality-select/equality-select.component'
import { FilterValue } from '../../models/FilterValue'
import { Column } from '../../models/Column'

@Component({
  selector: 'app-boolean-filter',
  standalone: true,
  imports: [
    CommonModule,
    ColumnsSelectComponent,
    EqualitySelectComponent,
    MatFormFieldModule,
    MatCheckboxModule,
  ],
  templateUrl: './boolean-filter.component.html',
  styleUrl: './boolean-filter.component.css',
})
export class BooleanFilterComponent {
  @Input() columns?: Column[]
  @Input() filterValue?: FilterValue
  @Output() valueChange = new EventEmitter<FilterValue>()

  inputValue: string = ''

  ngOnInit() {
    if (this.filterValue && this.filterValue.value === undefined) {
      this.valueChange.emit({
        ...this.filterValue,
        equality: 'equals',
        value: 'false',
      })
    }
  }

  checkboxChange(event: MatCheckboxChange) {
    if (!this.filterValue) {
      return
    }

    this.emitValue({
      ...this.filterValue,
      value: `${event.checked}`,
    })
  }

  emitValue(value: FilterValue) {
    if (!this.filterValue) {
      return
    }

    this.valueChange.emit({
      ...this.filterValue,
      ...value,
    })
  }
}

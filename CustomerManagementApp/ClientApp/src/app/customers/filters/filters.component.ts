import { CommonModule } from '@angular/common'
import { Component, Inject } from '@angular/core'
import { MatButtonModule } from '@angular/material/button'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatOptionModule } from '@angular/material/core'
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatGridListModule } from '@angular/material/grid-list'
import { MatIconModule } from '@angular/material/icon'
import { MatInputModule } from '@angular/material/input'
import { MatSelectChange, MatSelectModule } from '@angular/material/select'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatToolbarModule } from '@angular/material/toolbar'
import { EditableLabelComponent } from 'src/app/editable-label/editable-label.component'
import { InputPromptComponent } from 'src/app/input-prompt/input-prompt.component'
import { SavedFilters } from 'src/app/models/SavedFilters'
import { MetadataService } from 'src/app/services/metadata.service'
import { Column } from '../../models/Column'
import { FilterValue } from '../../models/FilterValue'
import { BooleanFilterComponent } from '../boolean-filter/boolean-filter.component'
import { ColumnsSelectComponent } from '../columns-select/columns-select'
import { DateFilterComponent } from '../date-filter/date-filter.component'
import { InputFilterComponent } from '../input-filter/input-filter.component'
import { SavedFiltersComponent } from '../saved-filters/saved-filters.component'
import { AuthenticationService } from 'src/app/services/authentication.service'
import { combineLatest } from 'rxjs'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { generateUUID } from 'src/app/utils'

@Component({
  selector: 'app-filters',
  standalone: true,
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatGridListModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatOptionModule,
    MatInputModule,
    MatSelectModule,
    InputFilterComponent,
    BooleanFilterComponent,
    DateFilterComponent,
    ColumnsSelectComponent,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    SavedFiltersComponent,
    EditableLabelComponent,
    InputPromptComponent,
    MatProgressSpinnerModule,
  ],
  templateUrl: './filters.component.html',
  styleUrl: './filters.component.css',
})
export class FiltersComponent {
  private restaurantId: string = ''

  columns: Column[] = []
  filters: FilterValue[] = []
  activeSavedFilters?: SavedFilters
  globalEquality: 'or' | 'and' = 'or'
  isDrawerOpen: boolean = false

  constructor(
    public metadataService: MetadataService,
    public auth: AuthenticationService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    if (data.columns) {
      this.columns = [...data.columns]
    }
  }

  ngOnInit() {
    combineLatest([
      this.metadataService.metadata$,
      this.metadataService.savedFilters$,
    ]).subscribe(([metadata, savedFilters]) => {
      this.activeSavedFilters = savedFilters[metadata.activeSavedFiltersIndex]
      this.activeSavedFilters = this.activeSavedFilters ?? savedFilters[0]
      if (this.activeSavedFilters) {
        this.filters = this.activeSavedFilters.filters ?? []
      }
      this.globalEquality = metadata.globalEquality ?? 'or'
    })

    this.auth.userInfo.subscribe((userinfo) => {
      this.restaurantId = userinfo?.restaurantId ?? ''
    })
  }

  addFilter() {
    this.filters.push({
      id: this.filters.length,
      equality: 'equals',
    })
    this.metadataService.updateColumnFilters(this.filters)
  }

  removeFilter(filterId: number) {
    this.filters = this.filters.filter((f) => f.id !== filterId)
    this.metadataService.updateColumnFilters(this.filters)
  }

  valueChange(value: FilterValue) {
    const filter = this.filters.find((f) => f.id === value.id)
    if (!filter) {
      return
    }

    filter.columnProps = value.columnProps
    filter.equality = value.equality
    filter.value = value.value
    this.metadataService.updateColumnFilters(this.filters)
  }

  globalEqualityChange(event: MatSelectChange) {
    this.globalEquality = event.value
    this.metadataService.updateMetadata(
      { globalEquality: this.globalEquality },
      true,
    )
  }

  saveFilters() {
    const savedFilters = this.metadataService.savedFilters$.getValue()
    if (!savedFilters.length) {
      this.duplicateFilters(this.metadataService.defaultSavedFiltersName)
      return
    }
    this.metadataService.saveFilters(this.restaurantId)
  }

  duplicateFilters(newName: string) {
    const newSavedFilters: SavedFilters = {
      id: generateUUID(),
      name: newName,
      filters: this.filters,
      createdAt: new Date(),
      updatedAt: new Date(),
    }
    this.metadataService.duplicateFilters(this.restaurantId, newSavedFilters)
  }

  renameFilters(newName:string) {
    this.metadataService.renameSavedFilters(this.restaurantId, newName)
  }
}

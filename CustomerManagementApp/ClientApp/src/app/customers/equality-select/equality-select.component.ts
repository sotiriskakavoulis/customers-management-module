import { Component, EventEmitter, Input, Output } from '@angular/core'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatSelectChange, MatSelectModule } from '@angular/material/select'
import { Equality } from '../../models/Equality'
import { ColumnType } from 'src/app/models/ColumnType'
import { NgIf } from '@angular/common'

@Component({
  selector: 'app-equality-select',
  standalone: true,
  imports: [MatFormFieldModule, MatSelectModule, NgIf],
  templateUrl: './equality-select.component.html',
  styleUrl: './equality-select.component.css',
})
export class EqualitySelectComponent {
  @Input() value?: Equality = 'equals'
  @Input() type?: ColumnType
  @Output() valueChange = new EventEmitter<Equality>()

  changeEquality(event: MatSelectChange) {
    this.valueChange.emit(event.value)
  }
}

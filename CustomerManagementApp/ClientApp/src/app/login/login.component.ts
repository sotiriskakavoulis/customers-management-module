import { Component } from '@angular/core';
import { LoginFormComponent } from './login-form/login-form.component';
import { AuthenticationService } from '../services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [LoginFormComponent],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class LoginComponent {
  constructor(
    private authentication: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  username: string | undefined;
  password: string | undefined;

  async ngOnInit(): Promise<void> {
    if (await this.authentication.isAuthenticated()) {
      this.router.navigate(['/customers']);
      return;
    }

    // Autologin from url
    this.username = this.route.snapshot.queryParams['user'];
    this.password = this.route.snapshot.queryParams['pass'];
  }
}

import { CommonModule } from '@angular/common';
import { AuthenticationService } from '../../services/authentication.service';
import { Component, Input } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  standalone: true,
  imports: [CommonModule, HttpClientModule, FormsModule],
  templateUrl: './login-form.component.html',
  styleUrl: './login-form.component.scss',
})
export class LoginFormComponent {
  @Input()
  username: string | undefined;
  @Input()
  password: string | undefined;
  errors: string[] = [];
  loggingIn: boolean = false;

  constructor(
    private loginService: AuthenticationService,
    private router: Router
  ) {}

  ngOnChanges(): void {
    if (this.username && this.password) {
      this.login(this.username, this.password);
    }
  }

  async login(username?: string, password?: string) {
    this.errors = [];
    this.loggingIn = true;

    if (!username || !password) {
      this.errors = ['Username and password are required.'];
      return;
    }
    const res = await this.loginService.login(username, password);

    if (res.success) {
      this.router.navigate(['/customers']);
    } else {
      this.errors = [res.errorMessage || 'Unknown error'];
    }
    this.loggingIn = false;
  }
}

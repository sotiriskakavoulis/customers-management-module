using CustomerManagementApp.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace CustomerManagementApp.DataAccess;

public class MarketingToolDbContext : DbContext
{
  public MarketingToolDbContext()
  {
  }

  public MarketingToolDbContext(DbContextOptions<MarketingToolDbContext> options) : base(options)
  {
  }

  public virtual DbSet<SavedFilters> SavedFilters { get; set; }
}

using CustomerManagementApp.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace CustomerManagementApp.DataAccess;

public class RestaurantDbContext : DbContext
{
  public RestaurantDbContext()
  {
  }

  public RestaurantDbContext(DbContextOptions<RestaurantDbContext> options) : base(options)
  {
  }

  public virtual DbSet<Customer> Customers { get; set; }
}

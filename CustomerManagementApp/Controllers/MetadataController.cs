using System.Security.Claims;
using CustomerManagementApp.DataAccess;
using CustomerManagementApp.Models;
using CustomerManagementApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CustomerManagementApp.Controllers;

[Authorize]
[Route("api/[controller]")]
[ApiController]
public class MetadataController : ControllerBase
{
  private const string RestaurantClaimType = "RestaurantClaimType";

  private readonly IMetadataService _metadataService;

  public MetadataController(IMetadataService metadataService, MarketingToolDbContext marketingToolDbContext)
  {
    _metadataService = metadataService;

    // marketingToolDbContext.Database.EnsureCreated();
    if (marketingToolDbContext.Database.GetPendingMigrations().Any())
      marketingToolDbContext.Database.Migrate();
  }

  // GET
  [HttpGet("{restaurantId}/saved_filters")]
  public async Task<ActionResult<List<SavedFilters>>> GetSavedFilters(string restaurantId)
  {
    var userRestaurantId = User.FindFirstValue(RestaurantClaimType);
    if (string.IsNullOrWhiteSpace(userRestaurantId) || !userRestaurantId.Equals(restaurantId))
      return BadRequest();

    var savedFilters = await _metadataService.GetSavedFiltersAsync(restaurantId);
    return Ok(savedFilters);
  }

  // POST
  [HttpPost("{restaurantId}/saved_filters")]
  public async Task<ActionResult<List<SavedFilters>>> SaveFilters(string restaurantId,
    [FromBody] List<SavedFilters> filters)
  {
    var userRestaurantId = User.FindFirstValue(RestaurantClaimType);
    if (string.IsNullOrWhiteSpace(userRestaurantId) || !userRestaurantId.Equals(restaurantId))
      return BadRequest();

    var savedFilters = await _metadataService.SaveFiltersAsync(restaurantId, filters);
    return Ok(savedFilters);
  }
}

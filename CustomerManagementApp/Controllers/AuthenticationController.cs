using System.Security.Claims;
using System.Text;
using System.Text.Json;
using CustomerManagementApp.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace CustomerManagementApp.Controllers;

[ApiController]
[Route("api/auth")]
public partial class AuthenticationController : ControllerBase
{
  private const string RestaurantClaimType = "RestaurantClaimType";
  private readonly HttpClient _httpClient;

  // Configure JsonSerializerOptions to ignore casing
  private readonly JsonSerializerOptions _options = new JsonSerializerOptions
  {
    PropertyNameCaseInsensitive = true
  };

  public AuthenticationController(IConfiguration configuration, IHttpClientFactory httpClientFactory)
  {
    _httpClient = httpClientFactory.CreateClient();
  }

  [HttpGet("is_authenticated")]
  public ActionResult<AuthenticationResponse> IsAuthenticated()
  {
    var authenticationResponse = GetAuthenticationResponse();
    authenticationResponse.Status = User.Identity?.IsAuthenticated ?? false;

    return Ok(authenticationResponse);
  }

  [HttpPost("login")]
  public async Task<ActionResult<AuthenticationResponse>> Login(
    [FromBody] LoginModel request)
  {
    var resp = await Authenticate(request.Username, request.Password);
    if (!resp.Status || resp.LoggedInemployee is null)
      return Ok(new ResponseStatus(resp));

    var user = resp.LoggedInemployee;
    var restaurant = resp.RestaurantsList?.FirstOrDefault();
    await SignInAsync(user, restaurant);

    var authenticationResponse = new AuthenticationResponse
    {
      Status = resp.Status,
      Name = user.Name,
      Surname = user.Surname,
      Email = user.Mail,
      RestaurantId = restaurant?.RestaurantId
    };
    return Ok(authenticationResponse);
  }

  [HttpPost("logout")]
  public async Task<ActionResult<ResponseStatus>> Logout()
  {
    await HttpContext.SignOutAsync();
    return Ok(new ResponseStatus
    {
      Status = true
    });
  }

  [HttpPost("access_denied")]
  public ActionResult<ResponseStatus> AccessDenied()
  {
    if (User.Identity?.IsAuthenticated == true)
      return Ok(new ResponseStatus
      {
        Status = true
      });

    return Unauthorized(new ResponseStatus
    {
      Status = false,
      ErrorCode = "Unauthorized",
      ErrorDescription = "You are not authorized to access this resource"
    });
  }

  #region Private methods

  private AuthenticationResponse GetAuthenticationResponse()
  {
    var authenticationResponse = new AuthenticationResponse()
    {
      Name = User.Identity?.Name,
      Surname = User.FindFirstValue(ClaimTypes.Surname),
      Email = User.FindFirstValue(ClaimTypes.Email),
      RestaurantId = User.FindFirstValue(RestaurantClaimType)
    };
    return authenticationResponse;
  }

  private async Task SignInAsync(LoggedInEmployee user, Restaurant? restaurant)
  {
    var claims = new List<Claim>
    {
      new Claim(ClaimTypes.Name, user.Name ?? string.Empty),
      new Claim(ClaimTypes.Surname, user.Surname ?? string.Empty),
      new Claim(ClaimTypes.Email, user.Mail ?? string.Empty),
      new Claim(RestaurantClaimType, restaurant?.RestaurantId ?? "")
      // Add additional claims as needed
    };

    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
    var authProperties = new AuthenticationProperties
    {
      // Set any additional properties as needed
    };

    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
      new ClaimsPrincipal(claimsIdentity), authProperties);
  }

  private async Task<LoginResponse> Authenticate(string username, string password)
  {
    // Build your object to POST
    var data = new
    {
      username = username,
      password = password
    };

    // Serialize your object to JSON
    var json = JsonSerializer.Serialize(data);
    // Encode the json as the content of your HTTP POST
    var content = new StringContent(json, Encoding.UTF8, "application/json");
    // Send the POST request
    _httpClient.DefaultRequestHeaders.Add("deviceid", "your-device-id");
    var response = await _httpClient.PostAsync("https://api-testing.i-host.gr/iAdmin/employees/logIn", content);

    // Get the response, you often want to check the response
    var responseContent = await response.Content.ReadAsStringAsync();
    if (response.IsSuccessStatusCode)
      return JsonSerializer.Deserialize<LoginResponse>(responseContent, _options)
             ?? new LoginResponse
             {
               Status = false,
               ErrorCode = "DeserializationError",
               ErrorDescription = "Could not deserialize response"
             };

    return new LoginResponse()
    {
      Status = false,
      ErrorCode = response.StatusCode.ToString(),
      ErrorDescription = response.ReasonPhrase
    };
  }

  #endregion
}

using CustomerManagementApp.Models;
using CustomerManagementApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace CustomerManagementApp.Controllers;

[Route("api/[controller]")]
public class FileController : ControllerBase
{
  private readonly IExportService _exportService;
  private readonly ICustomersService _customersService;

  public FileController(IExportService exportService, ICustomersService customersService)
  {
    _exportService = exportService;
    _customersService = customersService;
  }

  [HttpPost("for_restaurant/{restaurantId}")]
  public async Task<IActionResult> DownloadExcel(
    string restaurantId,
    [FromQuery] string? sortDirection = null,
    [FromQuery] string? sortProp = null,
    [FromBody] Filters? filters = null)
  {
    var data = await _customersService.CustomersByRestaurantAsync(
      restaurantId,
      null,
      null,
      sortDirection,
      sortProp,
      filters);
    var memoryStream = _exportService.GenerateExcelFile(data, filters!.EnabledColumns);

    return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "MyWorkbook.xlsx");
  }
}

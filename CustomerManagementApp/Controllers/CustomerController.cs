using CustomerManagementApp.Models;
using CustomerManagementApp.Models.Data;
using CustomerManagementApp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CustomerManagementApp.Controllers;

[Authorize]
[Route("api/[controller]")]
[ApiController]
public class CustomerController : ControllerBase
{
  private readonly ICustomersService _customersService;

  public CustomerController(ICustomersService customersService)
  {
    _customersService = customersService;
  }

  // GET a single customer
  [HttpGet("{customerId}")]
  public async Task<Customer?> GetCustomer(string customerId)
  {
    return await _customersService.FindCustomer(customerId);
  }

  // GET all customers of a specific restaurant
  [HttpPost("for_restaurant/{restaurantId}")]
  public async Task<CustomerListResponse> GetCustomers(
    string restaurantId,
    [FromQuery] int pageSize = 100,
    [FromQuery] int pageNumber = 100,
    [FromQuery] string? sortDirection = null,
    [FromQuery] string? sortProp = null,
    [FromBody] Filters? filters = null
  )
  {
    var data = await _customersService.CustomersByRestaurantAsync(
      restaurantId,
      pageSize,
      pageNumber,
      sortDirection,
      sortProp,
      filters);
    var totalElements = await _customersService.CountAsync(restaurantId, filters);

    return new CustomerListResponse
    {
      Data = data,
      Page = new Page
      {
        Size = pageSize,
        TotalElements = totalElements,
        TotalPages = totalElements / Math.Max(1, pageSize),
        PageNumber = pageNumber,
        SortDirection = sortDirection,
        SortProp = sortProp,
      }
    };
  }
}

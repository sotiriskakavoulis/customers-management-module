using CustomerManagementApp;
using CustomerManagementApp.DataAccess;
using CustomerManagementApp.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();

// Add HttpClientFactory
builder.Services.AddHttpClient();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var mainConnectionString = builder.Configuration.GetSection("MainConnectionString").Get<string>();
var serverVersion = ServerVersion.AutoDetect(mainConnectionString);
builder.Services.AddDbContext<RestaurantDbContext>(
  dbContextOptions => dbContextOptions.UseMySql(mainConnectionString, serverVersion)
  // The following three options help with debugging, but should
  // be changed or removed for production.
  // .LogTo(Console.WriteLine, LogLevel.Information)
  // .EnableSensitiveDataLogging()
  // .EnableDetailedErrors()
);
var marketingToolConnectionString = builder.Configuration.GetSection("MarketingToolConnectionString").Get<string>();
builder.Services.AddDbContext<MarketingToolDbContext>(
  dbContextOptions => dbContextOptions.UseMySql(marketingToolConnectionString, serverVersion)
);

builder.Services.AddScoped<ICustomersService, CustomersService>();
builder.Services.AddScoped<IExportService, ExportService>();
builder.Services.AddScoped<IMetadataService, MetadataService>();

//Authentication cookie
AuthenticationCookieSettings? jwtSettings = null;
jwtSettings = builder.Configuration.GetSection("AuthenticationCookieSettings").Get<AuthenticationCookieSettings>();
builder.Services.AddAuthentication(options =>
  {
    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
  })
  .AddCookie(options =>
  {
    options.Cookie.Name = jwtSettings?.CookieName ?? string.Empty; // Set your desired cookie name
    options.LoginPath = jwtSettings?.LoginPath ?? string.Empty; // Set the login page URL
    options.AccessDeniedPath = jwtSettings?.AccessDeniedPath ?? string.Empty; // Set the access denied page URL
    options.ExpireTimeSpan = TimeSpan.FromSeconds(jwtSettings?.AccessTokenExpiration ?? 2400);
    options.Events = new CookieAuthenticationEvents
    {
      OnRedirectToAccessDenied = context =>
      {
        // Set the response status code to 401 Unauthorized
        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
        return Task.CompletedTask;
      }
    };
  });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
  // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
  app.UseHsts();
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseAuthorization();

app.MapControllerRoute(
  name: "default",
  pattern: "{controller}/{action=Index}/{id?}");

if (app.Environment.IsDevelopment())
{
  app.UseStaticFiles();
  app.MapFallbackToFile("index.html");
  app.UseSwagger();
  app.UseSwaggerUI(c =>
  {
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
    c.RoutePrefix = "api/swagger"; // Change the Swagger UI endpoint)
  });
}

app.Run();

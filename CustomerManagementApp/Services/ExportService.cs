using ClosedXML.Excel;
using CustomerManagementApp.Models.Data;

namespace CustomerManagementApp.Services;

public class ExportService : IExportService
{
  public MemoryStream GenerateExcelFile(Customer[] customers, Dictionary<string, bool>? enabledColumns)
  {
    // Creating a new workbook
    using var workbook = new XLWorkbook();
    // Adding a worksheet
    var worksheet = workbook.Worksheets.Add("Customers");

    var customerType = typeof(Customer);
    var properties = customerType.GetProperties();
    // Adding headers
    var currentColumn = 1;
    foreach (var prop in properties)
    {
      var propName = prop.Name.ToLowerInvariant();
      // Skip not enabled columns
      var isColumnEnabled = false;
      enabledColumns?.TryGetValue(propName, out isColumnEnabled);
      if (!isColumnEnabled)
        continue;

      worksheet.Cell(1, currentColumn).Value = prop.Name;
      worksheet.Cell(1, currentColumn).Style.Font.Bold = true;

      var currentRow = 2;
      foreach (var obj in customers)
      {
        worksheet.Cell(currentRow, currentColumn).Value = prop.GetValue(obj)?.ToString();
        // Add other properties as needed
        currentRow++;
      }

      currentColumn++;
    }

    // Adjust column widths
    // Deactivated for performance issues
    // worksheet.Columns().AdjustToContents();

    // Saving the workbook to a file
    var stream = new MemoryStream();
    // Save the workbook to the MemoryStream
    workbook.SaveAs(stream);
    stream.Flush();
    stream.Position = 0;

    return stream;
  }
}

using System.Linq.Dynamic.Core;
using CustomerManagementApp.Models;
using CustomerManagementApp.Models.Data;

namespace CustomerManagementApp.Services;

public static class Extensions
{
  private static readonly Dictionary<Equality, string> EqualityMapper =
    new()
    {
      {
        Equality.Equals, "=="
      },
      {
        Equality.Contains, ""
      },
      {
        Equality.GreaterThan, ">"
      },
      {
        Equality.GreaterThanOrEqual, ">="
      },
      {
        Equality.IsNot, "!="
      },
      {
        Equality.LessThan, "<"
      },
      {
        Equality.LessThanOrEqual, "<="
      },
    };

  public static IQueryable<Customer> FilterBy(this IQueryable<Customer> query, Filters? filter = null)
  {
    var filterText = filter?.FilterText;

    if (!string.IsNullOrWhiteSpace(filterText))
      query = query.Where(x =>
        (!string.IsNullOrWhiteSpace(x.Name) && x.Name.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Surname) && x.Surname.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Telephone) && x.Telephone.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Mail) && x.Mail.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Typeofcustomer) && x.Typeofcustomer.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Note) && x.Note.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.HomePhone) && x.HomePhone.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.OfficePhone) && x.OfficePhone.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Country) && x.Country.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.City) && x.City.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Region) && x.Region.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Address) && x.Address.Contains(filterText))
        || (!string.IsNullOrWhiteSpace(x.Zipcode) && x.Zipcode.Contains(filterText))
      );

    if (!(filter?.ColumnFilters?.Length > 0))
      return query;

    var dynamicFilters = filter.ColumnFilters
      .Select(GenerateDynamicQuery)
      .Where(s => !string.IsNullOrWhiteSpace(s));
    var logicalOperator = filter.GlobalEquality == GlobalEquality.Or ? " OR " : " AND ";
    var dynamicQuery = string.Join(logicalOperator, dynamicFilters);

    // Applying the dynamic query
    return query.Where(dynamicQuery);
  }

  public static IQueryable<Customer> SortBy(this IQueryable<Customer> query, string? sortProp, string? sortDirection)
  {
    if (string.IsNullOrWhiteSpace(sortProp))
      return query.OrderBy(c => c.CreatedTime);

    var direction = sortDirection == "asc" ? "ASC" : "DESC";
    query = query.OrderBy($"{sortProp.MapSortingProperty()} {direction}");

    return query;
  }

  private static string? MapSortingProperty(this string? propertyName)
  {
    return propertyName?.ToLowerInvariant() switch
    {
      "lastvisit" => "LastVisitRaw",
      "timeperformed" => "TimePerformedRaw",
      _ => propertyName
    };
  }

  private static string? MapValue(string? propertyName, string? value)
  {
    if (string.IsNullOrWhiteSpace(value))
      return value;

    return propertyName?.ToLowerInvariant() switch
    {
      "lastvisit" => value.ToEpochTime(),
      "timeperformed" => value.ToEpochTime(),
      _ => value
    };
  }

  private static string? ToEpochTime(this string? value)
  {
    if (string.IsNullOrWhiteSpace(value))
      return value;

    var date = DateTime.Parse(value);
    // Convert to Unix time
    return ((DateTimeOffset)date).ToUnixTimeMilliseconds().ToString();
  }

  private static string GenerateDynamicQuery(ColumnFilter filter)
  {
    var prop = filter.Property.MapSortingProperty();
    var equality = filter.Equality.HasValue ? EqualityMapper[filter.Equality.Value] : "equals";
    var value = MapValue(prop, filter.Value);

    if (string.IsNullOrWhiteSpace(prop))
      return string.Empty;

    return filter.Equality == Equality.Contains
      ? $"{prop}.Contains(\"{value}\")"
      : $"{prop} {equality} \"{value}\"";
  }
}

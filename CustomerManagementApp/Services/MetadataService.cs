using CustomerManagementApp.DataAccess;
using CustomerManagementApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CustomerManagementApp.Services;

public class MetadataService : IMetadataService
{
  private readonly MarketingToolDbContext _context;

  public MetadataService(MarketingToolDbContext context)
  {
    _context = context;
  }

  public async Task<List<SavedFilters>> GetSavedFiltersAsync(string restaurantId)
  {
    var filtersInDb = await _context.SavedFilters
      .Where(f => f.RestaurantId == restaurantId)
      .Select(f => MapSavedFilters(f))
      .ToListAsync();

    return filtersInDb;
  }

  public async Task<List<SavedFilters>> SaveFiltersAsync(string restaurantId, List<SavedFilters> filters)
  {
    await DeleteExistingFiltersAsync(restaurantId);

    var updatedFilters = filters.Select(f => MapSavedFilters(restaurantId, f));
    await _context.SavedFilters.AddRangeAsync(updatedFilters);
    await _context.SaveChangesAsync();

    return filters;
  }

  #region Private Methods

  private async Task DeleteExistingFiltersAsync(string restaurantId)
  {
    var filtersInDb = await _context.SavedFilters
      .Where(f => f.RestaurantId == restaurantId)
      .ToListAsync();
    _context.SavedFilters.RemoveRange(filtersInDb);
    await _context.SaveChangesAsync();
  }

  private static Models.Data.SavedFilters MapSavedFilters(string restaurantId, SavedFilters filters)
  {
    var updatedFilters = new Models.Data.SavedFilters
    {
      RestaurantId = restaurantId,
      Name = filters.Name,
      CreatedAt = filters.CreatedAt.ToUniversalTime(),
      UpdatedAt = filters.UpdatedAt.ToUniversalTime(),
      Filters = JsonConvert.SerializeObject(filters.Filters),
      ReadOnly = filters.ReadOnly
    };
    return updatedFilters;
  }

  private static SavedFilters MapSavedFilters(Models.Data.SavedFilters filtersInDb)
  {
    return new SavedFilters
    {
      Id = filtersInDb.Id,
      Name = filtersInDb.Name,
      Filters = JsonConvert.DeserializeObject<List<FilterValue>>(filtersInDb.Filters) ?? new List<FilterValue>(),
      CreatedAt = filtersInDb.CreatedAt.ToLocalTime(),
      UpdatedAt = filtersInDb.UpdatedAt.ToLocalTime(),
      ReadOnly = filtersInDb.ReadOnly
    };
  }

  #endregion
}

using CustomerManagementApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace CustomerManagementApp.Services;

public interface IMetadataService
{
  Task<List<SavedFilters>> GetSavedFiltersAsync(string restaurantId);
  Task<List<SavedFilters>> SaveFiltersAsync(string restaurantId, List<SavedFilters> filters);
}

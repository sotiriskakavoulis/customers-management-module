using CustomerManagementApp.DataAccess;
using CustomerManagementApp.Models;
using CustomerManagementApp.Models.Data;
using Microsoft.EntityFrameworkCore;

namespace CustomerManagementApp.Services;

public class CustomersService : ICustomersService
{
  private readonly RestaurantDbContext _dbContext;

  public CustomersService(RestaurantDbContext dbContext)
  {
    _dbContext = dbContext;
  }

  public async Task<Customer?> FindCustomer(string customerId)
  {
    return await _dbContext.Customers.FindAsync(customerId).ConfigureAwait(false);
  }

  public async Task<Customer[]> CustomersByRestaurantAsync(string restaurantId, int? pageSize, int? pageNumber,
    string? sortDirection = "asc", string? sortProp = null, Filters? filter = null)
  {
    var query = _dbContext.Customers
      .Where(c => c.RestaurantId == restaurantId)
      .FilterBy(filter)
      .SortBy(sortProp, sortDirection);

    if (pageSize.HasValue && pageNumber.HasValue)
      query = query.Skip(pageSize.Value * pageNumber.Value);
    if (pageSize.HasValue)
      query = query.Take(pageSize.Value);

    return await query.ToArrayAsync();
  }


  public async Task<int> CountAsync(string restaurantId, Filters? filter = null)
  {
    return await _dbContext.Customers
      .FilterBy(filter)
      .CountAsync(x => x.RestaurantId == restaurantId);
  }
}

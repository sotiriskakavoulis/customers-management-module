using CustomerManagementApp.Models.Data;

namespace CustomerManagementApp.Services;

public interface IExportService
{
  MemoryStream GenerateExcelFile(Customer[] customers, Dictionary<string, bool>? enabledColumns);
}

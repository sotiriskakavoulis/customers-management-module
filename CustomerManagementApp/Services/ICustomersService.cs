using CustomerManagementApp.Models;
using CustomerManagementApp.Models.Data;

namespace CustomerManagementApp.Services;

public interface ICustomersService
{
  Task<Customer?> FindCustomer(string customerId);

  Task<Customer[]> CustomersByRestaurantAsync(string restaurantId, int? pageSize = null, int? pageNumber = null,
    string? sortDirection = null, string? sortProp = null, Filters? filter = null);

  Task<int> CountAsync(string restaurantId, Filters? filter);
}

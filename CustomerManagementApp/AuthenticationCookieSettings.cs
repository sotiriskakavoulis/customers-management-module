namespace CustomerManagementApp;

public class AuthenticationCookieSettings
{
  public string? CookieName { get; set; }
  public string? LoginPath { get; set; }
  public string? AccessDeniedPath { get; set; }
  public int? AccessTokenExpiration { get; set; }
}

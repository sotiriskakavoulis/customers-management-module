using System.Globalization;
using CustomerManagementApp.DataAccess;
using CustomerManagementApp.Models;
using CustomerManagementApp.Models.Data;
using CustomerManagementApp.Services;
using Moq;
using Moq.EntityFrameworkCore;

namespace CustomerManagementApp.Tests;

public class FilterByTests
{
  private readonly List<Customer> _data = new List<Customer>
  {
    new Customer
    {
      Name = "FirstName",
      Surname = "FirstSurname",
      Deleted = false,
      CancelledCounter = 1,
      CreatedTime = new DateTime(2024, 1, 15, 13, 00, 00)
    },
    new Customer
    {
      Name = "SecondName",
      Surname = "SecondSurname",
      Deleted = true,
      CancelledCounter = 3,
      CreatedTime = new DateTime(2024, 1, 07, 20, 00, 00)
    },
    new Customer
    {
      Name = "ThirdName",
      Surname = "ThirdSurname",
      Deleted = false,
      CancelledCounter = 4,
      CreatedTime = new DateTime(2023, 11, 07, 20, 00, 00)
    },
  };

  private readonly RestaurantDbContext _dbContext;

  public FilterByTests()
  {
    _dbContext = MockDbContext(_data);
  }

  [Fact]
  public void Filter_by_one_specific_name()
  {
    // Arrange
    const string name = "SecondName";
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter("name", Equality.Equals, name)
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.All(customers, c => Assert.Equal(name, c.Name));
  }

  [Fact]
  public void Filter_by_name_that_contains_text()
  {
    // Arrange
    const string searchTerm = "Name";
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter("name", Equality.Contains, searchTerm)
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Count() > 1);
  }

  [Fact]
  public void Filter_by_name_by_two_search_terms()
  {
    // Arrange
    const string searchTerm1 = "First";
    const string searchTerm2 = "Second";
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter("name", Equality.Contains, searchTerm1),
        new ColumnFilter("name", Equality.Contains, searchTerm2)
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Count() == 2);
  }

  [Fact]
  public void Filter_by_two_names()
  {
    // Arrange
    const string name1 = "FirstName";
    const string name2 = "SecondName";
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter("name", Equality.Equals, name1),
        new ColumnFilter("name", Equality.Equals, name2)
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Any(c => c.Name == name1));
    Assert.True(customers.Any(c => c.Name == name2));
  }

  [Fact]
  public void Filter_by_name_or_surname_same_entity()
  {
    // Arrange
    const string name = "FirstName";
    const string surname = "FirstSurname";
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter("name", Equality.Equals, name),
        new ColumnFilter("surname", Equality.Equals, surname)
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Any(c => c.Name == name && c.Surname == surname));
  }

  [Fact]
  public void Filter_by_name_or_surname_different_entities()
  {
    // Arrange
    const string name = "FirstName";
    const string surname = "SecondSurname";
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter("name", Equality.Equals, name),
        new ColumnFilter("surname", Equality.Equals, surname)
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Any(c => c.Name == name));
    Assert.True(customers.Any(c => c.Surname == surname));
  }

  [Fact]
  public void Filter_by_not_name()
  {
    // Arrange
    const string excludedName = "FirstName";
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter("name", Equality.IsNot, excludedName),
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Any());
    Assert.True(customers.All(c => c.Name != excludedName));
  }

  [Fact]
  public void Filter_by_exact_date()
  {
    // Arrange
    var expectedCreatedDate = new DateTime(2024, 1, 15, 13, 00, 00);
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "createdTime",
          Equality.Equals,
          expectedCreatedDate.ToString(CultureInfo.InvariantCulture))
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Any());
    Assert.True(customers.All(c => c.CreatedTime == expectedCreatedDate));
  }

  [Fact]
  public void Filter_by_greater_than_date()
  {
    // Arrange
    var startDate = new DateTime(2024, 1, 1, 0, 00, 00);
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "createdTime",
          Equality.GreaterThan,
          startDate.ToString(CultureInfo.InvariantCulture))
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Any());
    Assert.True(customers.All(c => c.CreatedTime > startDate));
  }

  [Fact]
  public void Filter_by_less_than_date()
  {
    // Arrange
    var startDate = new DateTime(2024, 1, 1, 0, 00, 00);
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "createdTime",
          Equality.LessThan,
          startDate.ToString(CultureInfo.InvariantCulture))
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.Any());
    Assert.True(customers.All(c => c.CreatedTime < startDate));
  }

  [Fact]
  public void Filter_by_less_than_date_no_customers_found()
  {
    // Arrange
    var startDate = new DateTime(2023, 1, 1, 0, 00, 00);
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "createdTime",
          Equality.LessThan,
          startDate.ToString(CultureInfo.InvariantCulture))
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(!customers.Any());
  }

  [Fact]
  public void Filter_by_exact_cancelled_counter()
  {
    // Arrange
    var expectedCount = 1;
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "cancelledCounter",
          Equality.Equals,
          expectedCount.ToString())
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.All(c => c.CancelledCounter == expectedCount));
  }

  [Fact]
  public void Filter_by_greater_than_cancelled_counter()
  {
    // Arrange
    var expectedCount = 2;
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "cancelledCounter",
          Equality.GreaterThan,
          expectedCount.ToString())
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.All(c => c.CancelledCounter >= expectedCount));
  }

  [Fact]
  public void Filter_by_less_than_cancelled_counter()
  {
    // Arrange
    var expectedCount = 2;
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "cancelledCounter",
          Equality.LessThan,
          expectedCount.ToString())
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.All(c => c.CancelledCounter <= expectedCount));
  }

  [Fact]
  public void Filter_by_not_equal_cancelled_counter()
  {
    // Arrange
    var expectedCount = 2;
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "cancelledCounter",
          Equality.IsNot,
          expectedCount.ToString())
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.All(c => c.CancelledCounter != expectedCount));
  }

  [Fact]
  public void Filter_by_deleted()
  {
    // Arrange
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "deleted",
          Equality.Equals,
          true.ToString())
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.All(c => c.Deleted ?? false));
  }

  [Fact]
  public void Filter_by_not_deleted()
  {
    // Arrange
    var filters = new Filters
    {
      ColumnFilters = new[]
      {
        new ColumnFilter(
          "deleted",
          Equality.Equals,
          false.ToString())
      }
    };

    // Act
    var customers = _dbContext.Customers.FilterBy(filters);

    // Assert
    Assert.True(customers.All(c => c.Deleted.HasValue && !c.Deleted.Value));
  }

  private static RestaurantDbContext MockDbContext(IEnumerable<Customer> data)
  {
    var mockContext = new Mock<RestaurantDbContext>();
    mockContext.Setup(x => x.Customers).ReturnsDbSet(data);
    var dbContext = mockContext.Object;
    return dbContext;
  }
}
